function initbis(){
	alert("Tu changes les cartes !");
	location.reload();
}
function init(){
	
	// Gestion du scrolling
	//var x=document.getElementById('choix');
	document.getElementById('affichageChoix').innerHTML=('Nombre d images :'+x.value);
	choixImage=x.value;
	
	var i = 0;
	var compteur = 1;

	//-- Initialisation des tableaux dynamiques -->

	motifsCartes = motifsCartes.splice(0, motifsCartes.length);
	etatsCartes = etatsCartes.splice(0, etatsCartes.length);
	
	while ( i < (2*x.value)){
	motifsCartes[i] = compteur;
	motifsCartes[i+1] = compteur;
	etatsCartes[i] = etatsCartes[i+1] =0;
	compteur++;
	i+=2;
	}
	// Création Table dynamique
	var myTableDiv = document.getElementById("myDynamicTable");
	// Chargement du contenair contenant les tables et je vide son contenu
	while (myTableDiv.firstChild) {
	 myTableDiv.removeChild(myTableDiv.firstChild);
	}
  var table = document.createElement('TABLE');
  table.border = '1';
  table.id = 'TABDYN';
  var nombreCellule = 0;
  

  var tableBody = document.createElement('TBODY');
  table.appendChild(tableBody);

  for (var i = 0; i < 4; i++) {
    var tr = document.createElement('TR');
    tableBody.appendChild(tr);

    for (var j = 0; j < 6; j++) {
		if (nombreCellule==(2*choixImage)) {
			break;
		}
		var img = document.createElement('img');
		img.src = './medias/fondcarte.png';
		var td = document.createElement('TD');
			td.width = '75';
			td.appendChild(img);
			tr.appendChild(td);
			nombreCellule+=1;
	  
    }
  }
 
myTableDiv.appendChild(table);

var imgCartes=document.getElementById("TABDYN").getElementsByTagName("img");		
<!--On parcourt le tableau des objets des éléments img, chacun d'eux reçoit une fonction déclenchée par l'événement onclick.

<!--La fonction ainsi définie est exécutée à chaque fois que l'utilisateur clique sur l'image son rôle est d'appeller controleJeu avec le numéro de l'image cliquée.

for(var i=0;i<imgCartes.length;i++){
	imgCartes[i].noCarte=i; //Ajout de la propriété noCarte à l'objet img
	imgCartes[i].onclick=function(){
		controleJeu(this.noCarte);
	}                      
}	
	
}
var x=document.getElementById('choix');

var motifsCartes=[0];
//-- Le codage utilisé pour l'état des cartes est le suivant : 0 : face cachée 1 : face visible -1 : enlevée -->

//-- Au départ toutes les cartes sont présentées de dos. -->

var etatsCartes=[0]; 
//-- Tableau contenant les numéros des cartes retournées à un moment donné du jeu.-->
init();

var cartesRetournees=[];
<!--Cette variable contient le nombre de paires de cartes qui ont déjà été trouvées.-->

var nbPairesTrouvees=0;
<!--Le tableau imgCarte contient les objets des éléments img de l'interface utilisateur.
init();

var imgCartes=document.getElementById("TABDYN").getElementsByTagName("img");		
<!--On parcourt le tableau des objets des éléments img, chacun d'eux reçoit une fonction déclenchée par l'événement onclick.

<!--La fonction ainsi définie est exécutée à chaque fois que l'utilisateur clique sur l'image son rôle est d'appeller controleJeu avec le numéro de l'image cliquée.

for(var i=0;i<imgCartes.length;i++){
	imgCartes[i].noCarte=i; //Ajout de la propriété noCarte à l'objet img
	imgCartes[i].onclick=function(){
		controleJeu(this.noCarte);
	}                      
}
<!--Appel de la fonction initialiseJeu pour mélanger les cartes.

initialiseJeu();
<!--La fonction majAffichage met à jour l'affichage de la carte dont on passe le numéro en paramètre.

<!--L'affichage rendu dépend de l'état actuel de la carte (donné par le tableau etatsCartes) :

<!--état 0 : carte face cachée, on affichage l'image de dos de carte : fondcarte.png,

<!--état 1 : carte retournée, on affiche l'image du motif correspondant, on notera que les différentes images des motifs sont dans les fichiers nommés carte1.png, carte2.png, etc.,

<!--état -1 : carte enlevée du jeu, on cache l'élément img.

function majAffichage(noCarte){
	switch(etatsCartes[noCarte]){
		case 0:
			imgCartes[noCarte].src="./medias/fondcarte.png";
			break;
		case 1:
			imgCartes[noCarte].src="./medias/carte"+motifsCartes[noCarte]+".png";
			break;
		case -1:
			imgCartes[noCarte].style.visibility="hidden";
			break;
	}
}
<!--La fonction rejouer affiche un message de félicitations et permet de jouer à nouveau en rechargeant la page dans le navigateur.

function rejouer(){
	alert("Bravo !");
	location.reload();
}
<!--La fonction initialiseJeu mélange les numéros de motif des cartes.

<!--Pour cela un algorithme de mélange est utilisé : [explications de l'algorithme](/programmation-en-javascript/melanger-les-elements-d-un-tableau/6)

function initialiseJeu(){
	for(var position=motifsCartes.length-1; position>=1; position--){
		var hasard=Math.floor(Math.random()*(position+1));
		var sauve=motifsCartes[position];
		motifsCartes[position]=motifsCartes[hasard];
		motifsCartes[hasard]=sauve;
	}
}
<!--C'est la fonction controleJeu qui contient le coeur du programme : elle est appelée chaque fois que l'utilisateur clique sur une carte en passant en paramètre le numéro de la carte cliquée.

function controleJeu(noCarte){
<!--Il est impossible d'avoir plus de deux cartes retournées en même temps, ce test évite que cela arrive, par exemple, si un utilisateur clique à toute vitesse sur plusieurs cartes.

	if(cartesRetournees.length<2){
<!--Si la carte cliquée est de dos (état 0) :

<!--on fait passer son état à 1,

<!--on ajoute son numéro au tableau des cartes retournées,

<!--on fait la mise à jour de son affichage.

<!--On notera que rien n'est fait pour les états 1 et -1 : cliquer sur une carte déjà retournée ne change rien et cliquer sur une zone de carte enlevée non plus.

		if(etatsCartes[noCarte]==0){
			etatsCartes[noCarte]=1;
			cartesRetournees.push(noCarte);
			majAffichage(noCarte);
		}
<!--Si on se retrouve avec deux cartes retournées, il faut déterminer si elles ont le même motif :

<!--si oui : les deux cartes prennent le nouvel état -1 (c'est à dire qu'il faut les enlever) et on incrémente la variable qui compte le nombre de paires trouvées (nbPairesTrouvees),

<!--si non : les deux cartes prennent le nouvel état 0 (c'est à dire qu'on les remet de dos).

		if(cartesRetournees.length==2){
			var nouveauEtat=0;
			if(motifsCartes[cartesRetournees[0]]==motifsCartes[cartesRetournees[1]]){
				nouveauEtat=-1;
				nbPairesTrouvees++;
			}

			etatsCartes[cartesRetournees[0]]=nouveauEtat;
			etatsCartes[cartesRetournees[1]]=nouveauEtat;
<!--Afin que le joueur ait le temps de voir ce qu'il se passe, on différe la mise à jour de l'affichage des cartes de 750 ms.

<!--Enfin au cas où toutes les paires ont été trouvées, on appelle la fonction rejouer

			setTimeout(function(){
				majAffichage(cartesRetournees[0]);
				majAffichage(cartesRetournees[1]);
				cartesRetournees=[];
				if(nbPairesTrouvees==choixImage){
					rejouer();
				}
			},750);
		}
	}
}